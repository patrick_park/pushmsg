#!/usr/bin/env ruby

require 'mysql2'
require 'cql'
require 'open-uri'
require 'net/http'
require 'uri'
require 'json'

module PushModule
  class PushMsg
    DB_MID_INFO = {:host=>'172.27.245.178', :port=>3306, :username=>'root', :password=>'rnrnrn', :database=>'mid', :reconnect=>true, :as=>:hash}
    DB_CONTENTS_INFO = {:host=>'172.27.216.234', :keyspace=>'contents', :credentials => {:username => 'bidulgi', :password=>'rnrnrncassandra'}}

    $sent_success_cnt = 0

    def connectCassandra
      return Cql::Client.connect(DB_CONTENTS_INFO)
    end

    def queryCql(query)
      begin
        if(!@cassandra_client)
          @cassandra_client = connectCassandra()
        end
        return @cassandra_client.execute(query)
      rescue Exception => e
        puts e
        sleep(5)
        if(@cassandra_client)
          @cassandra_client.close
          @cassandra_client = nil
        end
        retry
      end
    end

    def connectMysql
      return Mysql2::Client.new(DB_MID_INFO)
    end

    def querySql(query)
      begin
        if(!@client)
          @client = connectMysql()
        end
        return @client.query(query)
      rescue Exception => e
        puts e
        sleep(5)
        if(@client)
          @client.close
          @client = nil
        end
        retry
      end
    end
    
    def is_pushed(user_id, interval)
      query = "SELECT * FROM mobile_push 
        WHERE user_id = #{user_id} and pushed_date >= DATE_SUB(NOW(), INTERVAL #{interval} HOUR)"
      result = querySql(query)
      result.count == 0 ? false : true      
    end
   
    def pushed_user_success(user_id, success)
      query = "INSERT INTO pushed_user_list (user_id, success)
	       VALUES (#{user_id}, #{success})
	       ON DUPLICATE KEY UPDATE success = VALUES(success)"
      querySql(query)
    end

    def insert_pushed(user_id, title, contents)
      query = "INSERT INTO mobile_push (user_id, pushed_date, pushed_title, pushed_contents)
               VALUES (#{user_id}, NOW(), '#{title[0..49]}', '#{contents[0..49]}')"
      #puts query
      querySql(query)
    end

    def onesignal_push(user_id, title, contents)
      return if (user_id !=  12 ) # 박수환 user_id 일 경우만 보내기
      return if is_pushed(user_id, $push_term) == true # $push_term 시간 이내에 푸쉬 보냈으면 보내지 않는다

      params = {"app_id" => "44816446-831b-4b33-94e1-fc03d3967de4", 
                "headings" => {"en" => "Check your lenz articles!", "ko" => "#{title}"},         # 영어OS일 경우 제목
		"contents" => {"en" => "Tailored news and contents are ready", "ko" => "#{contents}"},   # 영어OS일 경우 본문
                "large_icon" => "http://14.63.166.45:3002/uploads/lenz_logo.png",
                "url" => "https://www.facebook.com/heylenz/posts/1122319441121778",   # 푸쉬 메세지 클릭했을 때 이동할 URL, 앱으로 이동시 삭제
                #"big_picture" => "http://14.63.166.45:3002/uploads/lenz_logo.jpg",
		"tags" => [{"key" => "user_id", "relation" => "=", "value" => "#{user_id}"}]}
      uri = URI.parse('https://onesignal.com/api/v1/notifications')
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      request = Net::HTTP::Post.new(uri.path,
				    'Content-Type'  => 'application/json',
				    'Authorization' => "Basic YzFkNWMwZTYtOGM4Yy00MmI0LWJkZjAtY2ZlMDU2MWI1ODk3")
      # 푸쉬 보낸 사람 기록
      insert_pushed(user_id, title, contents)
      request.body = params.to_json
      response = http.request(request) 
      result = response.body 
      puts result
 
      if JSON.parse(result)["id"] != nil # 푸쉬 전송 성공시
        $sent_success_cnt += 1 
        puts "푸쉬 성공한 유저수 : #{$sent_success_cnt}"
        # 푸쉬 성공한 유저 기록
        pushed_user_success(user_id, 1)
      else # 푸쉬 전송 실패시 
        pushed_user_success(user_id, 0)
      end
    end

    def get_candidate(taste_id)
      query = "SELECT id as user_id, name, sign_in_count, created_at, last_sign_in_at  FROM user_taste AS ut
        JOIN taste AS t ON t.taste_id = ut.taste_id
        JOIN user AS u ON u.id = ut.user_id
        WHERE ut.taste_id = #{taste_id} and follow = 1"
      puts query
      result = querySql(query)
    end

    def run

      $push_term = 0 # pushed_term 시간 이내 푸쉬 보낸 경우 다시 보내지 않는다

      ###########################
      # 트와이스	168434
      # 러블리즈	165317
      # 세븐틴		168465
      # IOI		171596
      # 방탄소년단	153502
      ###########################
 
      # 1. 어떤 키워드(taste_id)의 구독자에게 푸쉬 메세지를 보낼지 설정
      taste_id = 168434
      # 2. 푸쉬 메세지의 제목과 내용 설정
      title = "원스X트와이스X렌즈"
      contents = "트와이스 화력 지원하고 2집 앨범 받자!"

      user_list = get_candidate(taste_id)

      i = 0
      user_list.each do |r|
        user_id = r["user_id"]
        onesignal_push(user_id, title, contents)
        i+=1
        puts "(#{i}). Push msg for user_id = #{user_id}"
      end

      puts "푸쉬 성공한 유저수 : #{$sent_success_cnt}"
    end

    if __FILE__ == $0
      PushMsg.new.run
    end
  end
end
